// Instructions that can be provided to the students for reference:
// Activity:
// 1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a trainer object using object literals.
// 4. Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)

console.log("Pokemon Game");

let player1 = {
	name:"Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasur"],
	talk: function(monster){
		console.log(this.pokemon[monster] + "! I choose you!");
	}
}
console.log(player1);
console.log("Result of dot notation:");
console.log(player1.name);
console.log("Result of bracket notation:");
console.log(player1.pokemon);
console.log("Result of talk method:");
console.log(player1.talk(0));

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        if (target.health > 0 ) {
        	console.log( target.name + "'s health is now reduced to " + target.health);
        } else {
        	console.log( target.name + "'s health is now reduced to " + target.health);
        	console.log(target.name + ' fainted.');
        }
        
        };
    // this.faint = function(){
    //     console.log(this.name + 'fainted.');
    // }
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon('Geodude', 8); // 16 health
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);

// - Friends (Object with Array values for properties)
// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// 6. Access the trainer object properties using dot and square bracket notation.
// 7. Invoke/call the trainer talk object method.
// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)
// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// 11. Create a faint method that will print out a message of targetPokemon has fainted.
// 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// 13. Invoke the tackle method of one pokemon object to see if it works as intended.
// 14. Create a git repository named S23.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.

// let cellphone = {
// 	name: 'Nokia 3210',
// 	manufactureDate: 1999,
// 	price: 100
// };

// console.log('Result from creating objects');
// console.log(cellphone);
// console.log(typeof cellphone);
// function Laptop(name, manufactureDate, price){
// 	this.name = name;
// 	this.manufactureDate = manufactureDate;
// 	this.price = price
// }

// let laptop = new Laptop('Lenovo', 2008, 20000);
// console.log('Result from creating objects using object constructors:');
// console.log(laptop);

// let myLaptop = new Laptop('MacBook Air', 2020, 30000);
// console.log('Result from creating objects using object constructors:');
// console.log(myLaptop);


// [SECTION] Accessing Object Properties

// // Using the dot notation - for Best practice
// console.log('Result from dot notation: ' + myLaptop.name); 

// // Using the square bracket notation - Bad practice
// console.log('Result from square bracket notation: ' + myLaptop['name']);

// let array = [laptop, myLaptop];
// console.log(array[1].name);

// let sample = ['Hello',
//  {firstName: 'John', lastName: 'Doe'},
//   78];

// console.log(sample[1]);
// console.log(sample[1].lastName);

// let car = {};
// car.name = 'Honda Civic';
// console.log('Result from adding properties using dot notation:');
// console.log(car);

// car['manufacture date'] = 2019;
// console.log('Result from adding properties using bracket notation:')
// console.log(car);

// Deleting object properties
// delete car['manufacture date'];
// console.log('Result deleting properties:');
// console.log(car);
// // Reassigning object properties
// car.name = 'Dodge Charger R/T';
// console.log('Result from reassigning properties:')
// console.log(car);

// let person = {
//     name: 'John',
//     talk: function (){
//         console.log('Hello my name is ' + this.name);
//     }
// }

// console.log(person);
// console.log('Result from object methods:');
// person.talk();

// // Adding methods to objects
// person.walk = function() { 
//     console.log(this.name + ' walked 25 steps forward.');
// };
// person.walk();
// console.log(person)

// let friend = {
//     firstName: 'Joe',
//     lastName: 'Smith',
//     address: {
//         city: 'Austin',
//         country: 'Texas'
//     },
//     emails: ['joe@mail.com', 'joesmith@email.xyz'],
//     introduce: function() {
//         console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
//     }
// };
// friend.introduce();
// console.log(friend.address.city)
// console.log(friend.emails[0])
// console.log(friend.emails[1])

// [SECTION] Real World Application Objects
/*
	-Scenario
	1. We would like to create a game that would have pokemon interact with each other.
	2. Every pokemon would have the same set of stats, propeties and function.
*/






