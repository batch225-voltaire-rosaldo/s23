// [SECTION] Javascript Objects

/*
	- An object is a data type that is used to represent real world objects.
	- Information stored in objects are represented in a "key:value" pair
	- A 'key' is also mostly reffered to as a "property of an object"
	- Different data types may be stored in an object's property creating complex data structures.
	- Syntax: 
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999,
	price: 100
};

console.log('Result from creating objects.');
console.log(cellphone);
console.log(typeof cellphone);

// Constructors

// Syntax: 
	// function ObjectName(keyA, keyB) {
// 		this.keyA = keyA;
// 		this.keyB = keyB;
// }

function Laptop(name, manufactureDate, price){
	this.name = name;
	this.manufactureDate = manufactureDate;
	this.price = price;

	// return {name: name, manufactureDate: manufactureDate, price: price} // - use this if you dont declare "new" in objects
};

let laptop = new Laptop('Lenovo', 2008, 30000);
console.log(laptop);


let myLaptop = new Laptop('Macbook', 2020, 60000);
console.log(myLaptop);


// [SECTION] Accessing Object Properties
// Using the dot notation

console.log("Result from dot notation: " + myLaptop.name);

console.log("Result from bracket notation: " + myLaptop['name']); // using bracket notation. Possible alternative accessing of object property.

//Accessing Array objects

// let laptop = new Laptop('Lenovo', 2008, 30000);
// let myLaptop = new Laptop('Macbook', 2020, 60000);

let array = [laptop, myLaptop];

console.log(array[0]['price']); // bracket notation alternative accessing
console.log(array[1].name);

let sample = ['Hello', {fName: 'CJ', lName: 'Surname'}, 100];
console.log(sample[1]);
console.log(sample[1].fName);




















